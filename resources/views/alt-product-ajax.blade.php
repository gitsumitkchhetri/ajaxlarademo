@extends("alt-ajax-master")
@section("content")
    <div class="container">

        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">

                <h1>Products</h1>

                <p class="lead">This is a demo app with Laravel PHP and AJAX.</p>

                <form id="product_add">

                    <div class="messages" id="errormessage"></div>

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Product name *</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter name of the product *"  data-error="Product name is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_quantity">Quantity *</label>
                                    <input id="form_quantity" type="text" name="quantity" class="form-control" placeholder="Please enter quantity *"  data-error="Quantity is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_price">Price *</label>
                                    <input id="form_price" type="text" name="price" class="form-control" placeholder="Please enter the price *"  data-error="Valid number is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="btn btn-success btn-send" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-muted"><strong>*</strong> These fields are required.</p>
                            </div>
                        </div>
                    </div>

                </form>

            </div>

        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h1>List of Products</h1>
                <table class="table table-striped" id="product_table">
                    <thead class="text-primary">
                    <tr>
                        <th>Product name</th>
                        <th>Quantity</th>
                        <th>Price per unit</th>
                    </tr>

                    </thead>
                    <tbody id="mydata">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop