@extends("ajax-master")

@section("content")
    <div class="container">
        <div class="row main">
            <div class="main-login main-center">
                <h5>Sign up once and watch any of our free demos.</h5>
                <form class="" method="post" action="{{URL::to('storeProduct')}}"  id="myForm">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Name</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" id="name"  placeholder="Name"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Description</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" id="email"  placeholder="Description"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Price</label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="username" id="username"  placeholder="Price"/>
                            </div>
                        </div>
                    </div>



                    <div class="form-group ">
                        <button  type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script>
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $("#myForm").submit(function(event) {
//            return false;
            /* stop form from submitting normally */
            event.preventDefault();
            $.ajax({
                url: "{{URL::to('storeProduct')}}",
                dataType: 'json',
                type: 'post',
                //contentType: 'application/x-www-form-urlencoded',
                //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                //headers: {'X-CSRF-TOKEN': "{{csrf_field()}}"},
                data: $("#myForm").serialize(),
                success: function( data){
                   console.log(data);
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            });

        });

    </script>
@stop

