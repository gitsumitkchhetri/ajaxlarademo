<html>
<head>
    <title>Product</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

</head>
<body>

@yield("content")
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>




<script>

    $(document).ready(function() {
        $.ajax({
            url: "{{route('products.index')}}",
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (k,v) {
                    $("#mydata").append("<tr><td>"+v.name+"</td><td>"+v.quantity+"</td><td>"+v.price+"</td></tr>");
                });
                console.log(data);
            }

        });
    });

    $("form#product_add").submit(function () {
        event.preventDefault();
        var data = $("#product_add").serializeArray();
        var jdata = {};
        for (var i = 0; i < data.length; i++) jdata[data[i].name] = data[i].value;
        this.reset();
        view ='';

        $.ajax({
            url: "{{route('products.store')}}",
            headers: {
                'X-CSRF-TOKEN': "{{csrf_token()}}"
            },
            type: 'post',
            dataType: 'json',
            data: jdata,
            success: function (data) {
                console.log(data.error);
//                if(){
//
//                }

                $("#mydata").append("<tr><td>"+data.name+"</td><td>"+data.quantity+"</td><td>"+data.price+"</td></tr>");
                console.log(data);
            },
            error: function (xhr) {
                if(xhr.status==422) {
                    $('#errormessage').html("");
                    $.each(JSON.parse(xhr.responseText), function (k, v) {
                        $.each(v, function (key, value) {
                            $('#errormessage').append('<p style="color:red">' + value + '</p>');

                        })
                        console.log(view);
                    });
                }else if(xhr.status==501){
                    console.log(xhr.responseText);
                    $.each(JSON.parse(xhr.responseText), function (k, v) {
                        $('#errormessage').html(v[0]);
                    });
                } else if(xhr.status==500)
                    $('#errormessage').html("server error");

            }

        });

        return false;
    });

</script>

</body>
</html>