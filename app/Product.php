<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Validator;

class Product extends Model
{

    protected $table = "crm_addresses";
    protected $guarded = ['id', '_token'];

    protected $fillable = ['name','description','price'];
    protected $perPage = 30;

    protected $file_name = "product.json";

    public function writeToJson($data){
        if(Storage::has($this->file_name)){
            $inp = Storage::get($this->file_name);
            $tempArray = json_decode($inp,true);


            $tempArray[] =$data;
            $jsonData = json_encode($tempArray);
            Storage::put($this->file_name, $jsonData);
        }else{
            $dataArr = [];
            $dataArr[] = $data;
            Storage::put($this->file_name,json_encode($dataArr));
        }

        return true;


    }

    public function getJson(){
        $inp = Storage::get($this->file_name);

        return $inp;
    }

    public function validateProduct($fields){
        return $validator = Validator::make($fields, [
            'name' => 'required|string',
            'quantity' => 'required|numeric',
            'price' => 'required|numeric'
        ]);


    }

    public function getProductValidationResponse($messages){
        $messageStack = [];

        foreach ($messages as $message){
            foreach ($message as $m){
                $messageStack[] = $m;
            }
        }

        return $messageStack;
    }

}
